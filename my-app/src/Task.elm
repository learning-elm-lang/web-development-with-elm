module Task exposing (..)

import Task exposing (Task)
import Date exposing (Date)

succeeded : Task Never Int
succeeded =
    Task.succeed 1

failed : Task String x
failed =
    Task.fail "everything is broken"

now : Task x Date
now =
    Date.now
